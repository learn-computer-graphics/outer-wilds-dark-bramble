// Copyright (c) Contributors to the Learn Computer Graphics Project. SPDX-License-Identifier: MIT

#include "Interactibles/OWBoxPortal.h"

#include "Components/BoxComponent.h"

AOWBoxPortal::AOWBoxPortal(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UBoxComponent>(TEXT("CollisionComp")))
{
}
