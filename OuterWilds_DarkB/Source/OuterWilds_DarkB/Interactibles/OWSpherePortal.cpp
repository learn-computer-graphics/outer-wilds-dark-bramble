// Copyright (c) Contributors to the Learn Computer Graphics Project. SPDX-License-Identifier: MIT

#include "Interactibles/OWSpherePortal.h"

#include "Components/SphereComponent.h"

AOWSpherePortal::AOWSpherePortal(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<USphereComponent>(TEXT("CollisionComp")))
{
}
