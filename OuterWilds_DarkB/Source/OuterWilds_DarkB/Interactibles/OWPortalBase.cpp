// Copyright (c) Contributors to the Learn Computer Graphics Project. SPDX-License-Identifier: MIT

#include "Interactibles/OWPortalBase.h"

#include "Components/ArrowComponent.h"
#include "Components/ShapeComponent.h"
#include "Kismet/GameplayStatics.h"

AOWPortalBase::AOWPortalBase(const FObjectInitializer& ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	// AOWPortalBase is requesting UShapeComponent which is abstract, however it is responsibility
	// of a derived class to override this type with ObjectInitializer.SetDefaultSubobjectClass.
	CollisionComponent = CreateDefaultSubobject<UShapeComponent>(TEXT("CollisionComp"));
	if (CollisionComponent)
	{
		RootComponent = CollisionComponent;

		CollisionComponent->SetCollisionObjectType(ECC_WorldStatic);
		CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		CollisionComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

		CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AOWPortalBase::OnPortalBeginOverlap);
		CollisionComponent->OnComponentEndOverlap.AddDynamic(this, &AOWPortalBase::OnPortalEndOverlap);
	}

#if WITH_EDITORONLY_DATA
	ArrowComponent = CreateEditorOnlyDefaultSubobject<UArrowComponent>(TEXT("PortalForward"));
	ArrowComponent->SetupAttachment(RootComponent);
	ArrowComponent->ArrowColor = FColor(150, 200, 255);
	ArrowComponent->bHiddenInGame = false;
#endif
}

FRotator AOWPortalBase::ComputeActorDestinationRotation(AActor* Actor) const
{
	FQuat RelativeRotation = GetActorQuat().Inverse() * Actor->GetActorQuat();
	// Flip as both entry and exit are in front of the portal
	RelativeRotation = FQuat::MakeFromRotator({0, 180, 0}) * RelativeRotation;
	RelativeRotation = DestinationPortal->GetTransform().GetRotation() * RelativeRotation;
	return RelativeRotation.Rotator();
}

void AOWPortalBase::OnPortalBeginOverlap(UPrimitiveComponent* /*OverlappedComponent*/, AActor* OtherActor,
	UPrimitiveComponent* /*OtherComp*/, int32 /*OtherBodyIndex*/, bool /*bFromSweep*/, const FHitResult& /*SweepResult*/)
{
	const bool actorWasJustTeleportedHere = OverlappingActorsTeleportedToSelf.Contains(OtherActor);
	if (actorWasJustTeleportedHere)
		return;

	if (!DestinationPortal.IsValid())
	{
		UE_LOG(LogScript, Warning, TEXT("Destination portal not set for %s"), *GetName());
		return;
	}

	if (!DestinationPortal->TeleportActorToSelf(OtherActor, ComputeActorDestinationRotation(OtherActor)))
	{
		UE_LOG(LogScript, Warning, TEXT("Failed to teleport %s to %s"), *GetName(), *DestinationPortal->GetName())
	}
}

void AOWPortalBase::OnPortalEndOverlap(
	UPrimitiveComponent* /*OverlappedComponent*/, AActor* OtherActor, UPrimitiveComponent* /*OtherComp*/, int32 /*OtherBodyIndex*/)
{
	const bool ActorWasJustTeleportedHere = OverlappingActorsTeleportedToSelf.Contains(OtherActor);
	if (!ActorWasJustTeleportedHere)
		return;

	OverlappingActorsTeleportedToSelf.Remove(OtherActor);

	const auto PortalToPlayer = OtherActor->GetActorLocation() - GetActorLocation();
	const bool LeavesByBack = FVector::DotProduct(PortalToPlayer, CollisionComponent->GetForwardVector()) < 0;
	if (LeavesByBack)
	{
		DestinationPortal->TeleportActorToSelf(OtherActor, ComputeActorDestinationRotation(OtherActor));
	}
}

bool AOWPortalBase::TeleportActorToSelf(AActor* Actor, const FRotator& DestRotation)
{
	// #GH_TODO will need to fixup velocity as well
	OverlappingActorsTeleportedToSelf.Add(Actor);
	if (!Actor->TeleportTo(GetTransform().GetTranslation(), DestRotation))
	{
		OverlappingActorsTeleportedToSelf.Remove(Actor);
		return false;
	}

	if (APawn* Pawn = Cast<APawn>(Actor))
	{
		Pawn->GetController()->SetControlRotation(DestRotation);
	}

	return true;
}
