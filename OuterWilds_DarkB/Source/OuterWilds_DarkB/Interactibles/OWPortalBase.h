// Copyright (c) Contributors to the Learn Computer Graphics Project. SPDX-License-Identifier: MIT

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "OWPortalBase.generated.h"

class UShapeComponent;
class UArrowComponent;

UCLASS(Abstract)
class OUTERWILDS_DARKB_API AOWPortalBase : public AActor
{
	GENERATED_BODY()

public:
	AOWPortalBase(const FObjectInitializer& ObjectInitializer);

	bool TeleportActorToSelf(AActor* Actor, const FRotator& AdditionalRotation);

protected:
	UPROPERTY(EditDefaultsOnly, Category = Teleport)
	TObjectPtr<UShapeComponent> CollisionComponent;

	UPROPERTY(EditInstanceOnly, Category = Teleport)
	TSoftObjectPtr<AOWPortalBase> DestinationPortal;

	FRotator ComputeActorDestinationRotation(AActor* Actor) const;

private:
	UFUNCTION()
	void OnPortalBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnPortalEndOverlap(
		UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

#if WITH_EDITORONLY_DATA
	/** Arrow used to vizualize the TeleporterCollider forward vector */
	UPROPERTY()
	TObjectPtr<UArrowComponent> ArrowComponent;
#endif

	TSet<TWeakObjectPtr<AActor>> OverlappingActorsTeleportedToSelf;
};
