// Copyright (c) Contributors to the Learn Computer Graphics Project. SPDX-License-Identifier: MIT

#pragma once

#include "CoreMinimal.h"
#include "Interactibles/OWPortalBase.h"

#include "OWSpherePortal.generated.h"

UCLASS()
class OUTERWILDS_DARKB_API AOWSpherePortal : public AOWPortalBase
{
	GENERATED_BODY()

public:
	AOWSpherePortal(const FObjectInitializer& ObjectInitializer);
};
