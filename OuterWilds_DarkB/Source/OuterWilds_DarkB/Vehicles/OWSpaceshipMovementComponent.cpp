// Copyright (c) Contributors to the Learn Computer Graphics Project. SPDX-License-Identifier: MIT

#include "OWSpaceshipMovementComponent.h"

UOWSpaceshipMovementComponent::UOWSpaceshipMovementComponent()
{
	MaxSpeed = 4000.f;
	Acceleration = 5000.f;
	Deceleration = 8000.f;
	TurningBoost = 8.0f;

	ResetMoveState();
}

void UOWSpaceshipMovementComponent::TickComponent(
	float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PawnOwner || !UpdatedComponent)
	{
		return;
	}

	const FVector InputVector = ConsumeInputVector();
	if (InputVector.Size() > 0.f)
	{
		// Apply change in velocity direction
		if (Velocity.SizeSquared() > 0.f)
		{
			// Change direction faster than only using acceleration, but never increase velocity magnitude.
			const float TimeScale = FMath::Clamp(DeltaTime * TurningBoost, 0.f, 1.f);
			Velocity += (InputVector * Velocity.Size() - Velocity) * TimeScale;
		}
	}
	else if (Velocity.SizeSquared() > 0.f)
	{
		const float VelocityDecrease = FMath::Max(Velocity.Size() - FMath::Abs(Deceleration) * DeltaTime, 0.f);
		Velocity = Velocity.GetSafeNormal() * VelocityDecrease;
	}

	Velocity += InputVector * FMath::Abs(Acceleration) * DeltaTime;
	Velocity = Velocity.GetClampedToMaxSize(MaxSpeed);

	// Move actor
	FVector Delta = Velocity * DeltaTime;
	if (!Delta.IsNearlyZero(1e-6f))
	{
		FHitResult Hit(1.f);
		SafeMoveUpdatedComponent(Delta, UpdatedComponent->GetComponentQuat(), true, Hit);
		if (Hit.IsValidBlockingHit())
		{
			HandleImpact(Hit, DeltaTime, Delta);
			SlideAlongSurface(Delta, 1.f - Hit.Time, Hit.Normal, Hit, true);
		}
	}

	UpdateComponentVelocity();
}
