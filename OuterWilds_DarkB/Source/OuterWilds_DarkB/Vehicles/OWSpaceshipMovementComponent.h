// Copyright (c) Contributors to the Learn Computer Graphics Project. SPDX-License-Identifier: MIT

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"

#include "OWSpaceshipMovementComponent.generated.h"

// Based on UFloatingPawnMovement
UCLASS(ClassGroup = Movement, meta = (BlueprintSpawnableComponent))
class OUTERWILDS_DARKB_API UOWSpaceshipMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()

public:
	UOWSpaceshipMovementComponent();

	// Begin UActorComponent Interface
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	// End UActorComponent Interface

	// Begin UMovementComponent Interface
	virtual float GetMaxSpeed() const override
	{
		return MaxSpeed;
	}
	// End UMovementComponent Interface

private:
	UPROPERTY()
	float Acceleration;

	UPROPERTY(EditAnywhere)
	float MaxSpeed;

	UPROPERTY(EditAnywhere)
	float Deceleration;

	UPROPERTY(EditAnywhere)
	float TurningBoost;
};
