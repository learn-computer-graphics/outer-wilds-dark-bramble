// Copyright (c) Contributors to the Learn Computer Graphics Project. SPDX-License-Identifier: MIT

#include "Vehicles/OWSpaceshipPawn.h"

#include "Components/SphereComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "OWSpaceshipMovementComponent.h"

AOWSpaceshipPawn::AOWSpaceshipPawn()
{
	PrimaryActorTick.bCanEverTick = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	SphereComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	RootComponent = SphereComponent;

	SpaceshipMovement = CreateDefaultSubobject<UOWSpaceshipMovementComponent>(TEXT("SpaceshipMovComp"));
	SpaceshipMovement->UpdatedComponent = SphereComponent;

	bUseControllerRotationPitch = true;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = true;
}

void AOWSpaceshipPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AOWSpaceshipPawn::Move);
		EnhancedInputComponent->BindAction(MoveUpAction, ETriggerEvent::Triggered, this, &AOWSpaceshipPawn::MoveUp_World);
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AOWSpaceshipPawn::Look);
		EnhancedInputComponent->BindAction(RollAction, ETriggerEvent::Triggered, this, &AOWSpaceshipPawn::Roll);
	}
}

void AOWSpaceshipPawn::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem =
				ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

UPawnMovementComponent* AOWSpaceshipPawn::GetMovementComponent() const
{
	return SpaceshipMovement;
}

void AOWSpaceshipPawn::Move(const FInputActionValue& Value)
{
	FVector2D MovementVector = Value.Get<FVector2D>();
	if (Controller != nullptr)
	{
		FRotator const ControlSpaceRot = Controller->GetControlRotation();
		AddMovementInput(FRotationMatrix(ControlSpaceRot).GetScaledAxis(EAxis::X), MovementVector.Y);
		AddMovementInput(FRotationMatrix(ControlSpaceRot).GetScaledAxis(EAxis::Y), MovementVector.X);
	}
}

void AOWSpaceshipPawn::MoveUp_World(const FInputActionValue& Value)
{
	float Movement = Value.Get<float>();
	if (Controller != nullptr)
	{
		AddMovementInput(GetActorUpVector(), Movement);
	}
}

void AOWSpaceshipPawn::Look(const FInputActionValue& Value)
{
	FVector2D LookAxisVector = Value.Get<FVector2D>();
	if (Controller != nullptr)
	{
		AddActorLocalRotation(FQuat::MakeFromRotator({-LookAxisVector.Y, LookAxisVector.X, 0}));
		Controller->SetControlRotation(GetActorRotation());
	}
}

void AOWSpaceshipPawn::Roll(const FInputActionValue& Value)
{
	float Movement = Value.Get<float>();
	if (Controller != nullptr)
	{
		AddControllerRollInput(Movement);
	}
}
