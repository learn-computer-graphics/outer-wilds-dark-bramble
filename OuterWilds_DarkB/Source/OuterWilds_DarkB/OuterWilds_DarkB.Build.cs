// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class OuterWilds_DarkB : ModuleRules
{
    public OuterWilds_DarkB(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { 
            "Core", 
            "CoreUObject", 
            "Engine",
            "InputCore",
            "EnhancedInput"
        });

        PrivateDependencyModuleNames.AddRange(new string[] { });

        PublicIncludePaths.AddRange(new string[] {
            "OuterWilds_DarkB"
        });
    }
}
